import java.util.List;

/**
 * Created by tritoon on 12/08/16.
 */
public class Color {
    private double R,G,B;

    public Color(double r, double g, double b) {
        R = r;
        G = g;
        B = b;
    }

    public Color() {
        R = 0.5;
        G = 0.5;
        B = 0.5;
    }

    public double getR() {
        return R;
    }

    public double getG() {
        return G;
    }

    public double getB() {
        return B;
    }

    public void setR(double r) {
        R = r;
    }

    public void setG(double g) {
        G = g;
    }

    public void setB(double b) {
        B = b;
    }


    /**
     * Sets a color for each particle as a heat map, based on the number of neighbors and a global scale.
     * The color is a linear interpolation between green and red in its hexa max values.
     * @param particles
     */
    public static void setHeatColor(Particle particles, double maxAffected, double affected) {
        if(maxAffected == 0){
            return;
        }

        particles.getColor().setR((affected/maxAffected)*3);
        particles.getColor().setB(0);
        particles.getColor().setG((1- (affected/maxAffected)));
    }
}
