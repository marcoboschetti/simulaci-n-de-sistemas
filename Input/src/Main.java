
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by tritoon on 10/08/16.
 */
public class Main {

    public static final int MAX_TIME = 50000;

    public static void main(String[] args) {
        String staticFile = "staticTest1";
        String dinamicFile = "dinamicTest1";
        String fileXYZ = "status";
        String MfunctionFile = "timeFunction.csv";
        final int TRIES_PER_M = 5;
        int particles = 2500;
        int L = 200;
        double ETA = 0.1 * Math.PI;
        double rc = 10;
        double minRadius = 0;
        double maxRadius = 0;
        int maxM = (int) Math.floor((L / (rc + 2 * maxRadius)) - 1);
        boolean heatMap = false;

        //new StateGenerator().generateRandomState(L, particles, minRadius, maxRadius, staticFile, dinamicFile);

        //singleTest(L, maxM, rc, staticFile, dinamicFile, true, MAX_TIME, fileXYZ, ETA, heatMap);
        try {
            iterativeTest("ETAGraphPaperOriginal.csv", 750, 750, 3000, 500, maxM, 1, 0, 0, 10, staticFile, dinamicFile, fileXYZ, false);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void singleTest(int L, int M, double rc, String staticFile, String dinamicFile,
                                  boolean periodicCondition, int MAX_TIME, String fileXYZ, double ETA, boolean heatMap) {
        new Simulator().simulate(staticFile, dinamicFile, L, M, rc, periodicCondition, MAX_TIME, fileXYZ, ETA, heatMap);

    }

    public static void iterativeTest(String MfunctionFile, int initParticles, int stepParticles, int endParticles,
                                     int L, int M, double rc, double minRadius, double maxRadius, int TRIES_PER_M,
                                     String staticFile, String dinamicFile, String fileXYZ, boolean heatMap) throws ExecutionException, InterruptedException {
        PrintWriter writer = null;
        final int MAX_TIME = 500;

        try {
            writer = new PrintWriter(MfunctionFile, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        int[] partValues = {40, 100, 400, 4000, 10000};
        double[] lValues = {3.1, 5, 10, 31.6, 50};


        for (int i = 0; i < 5; i++) {
            final int particles = partValues[i];
            final double l = lValues[i];
            int m = (int) Math.floor((l / (rc + 2 * maxRadius)) - 1);
            writer.write(String.format("Particles: %d L: %f\nETA, avg, desvio\n", particles, l));
            System.out.print(String.format("Particles: %d L: %f\n", particles, l));
            for (double ETA = 0; ETA <= 2 * Math.PI; ETA += Math.PI / 5) {
                double avgVel = 0, sqrAvgVel = 0;
                System.out.println(String.format("ETA: %f", ETA));
                System.out.print(String.format("iteration "));
                //ExecutorService executorService = Executors.newFixedThreadPool(TRIES_PER_M);
                List<Future> futures = new ArrayList<>();
                for (int t = 0; t < TRIES_PER_M; t++) {
                    final int iteration = t;
                    final double currentETA = ETA;
                    //futures.add(executorService.submit(() -> {
                        new StateGenerator().generateRandomState(l, particles, minRadius, maxRadius,
                                staticFile + iteration, dinamicFile + iteration);
                        //Particle.resetId();
                        double currentAvgVel = new Simulator().simulate(staticFile + iteration, dinamicFile + iteration, l, m, rc, true,
                                MAX_TIME, fileXYZ, currentETA, heatMap);
                        System.out.print(String.format("%d, ", iteration));
                    avgVel += currentAvgVel;
                    sqrAvgVel += currentAvgVel * currentAvgVel;
                  //      return currentAvgVel;
                //    }));
                }
                /*/.
                for (Future future : futures) {
                    double currentAvgVel = (double) future.get();
                    avgVel += currentAvgVel;
                    sqrAvgVel += currentAvgVel * currentAvgVel;
                }
                */
            //executorService.shutdownNow();
            System.out.println();
                avgVel /= TRIES_PER_M;
                sqrAvgVel /= TRIES_PER_M;
                writer.write(String.format("%f, %f, %f\n", ETA, avgVel, sqrAvgVel - avgVel * avgVel));
            }
        }
        writer.close();
    }
}
