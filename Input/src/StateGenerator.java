import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * Created by tritoon on 11/08/16.
 */
public class StateGenerator {


    /**
     * Writes 2 files with an initial states for the particles as indicated in Simulator.
     * The position is greater or equals to 0 and less than L for each particle.
     * Superposition is not checked by the generator
     * @param L
     * @param particles
     * @param minRadius
     * @param maxRadius
     * @param staticFilename
     * @param dinamicFilename
     */
    public void generateRandomState(double L, int particles, double minRadius, double maxRadius, String staticFilename, String dinamicFilename){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(staticFilename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        for(int i=0 ; i < particles ; i++) {
            double radius = minRadius + Math.random() * (maxRadius-minRadius);
            writer.write(String.format("%f",radius));
            if(i < particles - 1){
                writer.write(",");
            }
        }
        writer.close();

        try {
            writer = new PrintWriter(dinamicFilename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        for(int i=0 ; i < particles ; i++) {
            double ang = Math.random() * 2 * Math.PI;
            writer.write(String.format("%f %f %f %f\n",Math.random() * L, Math.random() * L, Math.sin(ang) , Math.cos(ang)));
        }
        writer.close();


    }

}
