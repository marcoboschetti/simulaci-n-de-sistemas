import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by tritoon on 10/08/16.
 */
public class Simulator {

    private final boolean USE_ETA = true, USE_WEIGHTS = false;

    private Map<Particle, Set<Particle>> neighborMap;
    private List<Particle> particles;

    /**
     * @param staticData  File with particle radius, with format:
     *                    r1,r2,...,rn
     * @param dinamicData File with particle positions, with format:
     *                    x1 y1
     *                    ...
     *                    xn yn
     * @param L           Side of the simulation space
     * @param M           Number of cells in the Cell Index Method
     * @param rc          Influence radius for all particles
     * @param MAX_TIME
     * @param fileXYZ
     * @param heatMap
     */

    public double simulate(String staticData, String dinamicData, double L, int M, double rc,
                                  boolean periodicCondition, int MAX_TIME, String fileXYZ, double ETA, boolean heatMap) {

        particles = new ArrayList<>();
        double maxRadius = 0;

        try {
            BufferedReader staticInputBufferedReader = new BufferedReader(new FileReader(staticData));
            BufferedReader dinamicInputbufferedReader = new BufferedReader(new FileReader(dinamicData));

            String[] radiuses = staticInputBufferedReader.readLine().split(",");

            staticInputBufferedReader.close();

            for (String radius : radiuses) {
                double doubleRadius = Double.parseDouble(radius);
                String[] position;
                position = dinamicInputbufferedReader.readLine().split(" ");
                double x = Double.parseDouble(position[0]);
                double y = Double.parseDouble(position[1]);
                double velX = Double.parseDouble(position[2]);
                double velY = Double.parseDouble(position[3]);
                particles.add(new Particle(x, y, velX, velY, doubleRadius));
                if (doubleRadius > maxRadius) {
                    maxRadius = doubleRadius;
                }
            }
            dinamicInputbufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (L / M <= rc + 2 * maxRadius) {
            throw new IllegalArgumentException("Cell Index Method precondition not met. (L/M) should be greater than rc + 2* maxRad");
        }

        fileXYZ = String.format("%s_%sN%dL%.2fRC%.2fETA%.2f.xyz", heatMap ? "H" : "A", fileXYZ, particles.size(), L, rc, ETA);
        double starTime = System.currentTimeMillis();

        List<Particle>[][] matrix = new LinkedList[M][M];
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < M; j++) {
                matrix[i][j] = new LinkedList<>();
            }
        }

        double side = L / M;
        for (Particle particle : particles) {
            matrix[(int) Math.floor(particle.getX() / side)][(int) Math.floor(particle.getY() / side)].add(particle);
        }

        List<Particle> corners = new ArrayList<>();
        corners.add(new Particle(0, 0, 0.01, 0, 0));
        corners.add(new Particle(0, L, 0.01, 0, 0));
        corners.add(new Particle(L, 0, 0.01, 0, 0));
        corners.add(new Particle(L, L, 0.01, 0, 0));


        //PrinterXYZ.startPrinting(fileXYZ);

        for (int time = 0; time < MAX_TIME; time++) {
            //System.out.println("Time: " + time);
            checkInfluence(matrix, rc, L, periodicCondition);
            updateStates(ETA);
            moveParticles(matrix, L, side);

            if (heatMap) {
                ColorManager.setHeatColor(neighborMap, particles);
            } else {
            //    ColorManager.setAngleColor(particles);
            }

            //PrinterXYZ.printTimeStep(particles, corners, time);
        }

        //PrinterXYZ.endPrinting();

        double endTime = System.currentTimeMillis();
        double totalVelX = 0, totalVelY = 0;
        for (Particle particle : particles) {
            totalVelX += particle.getVelX();
            totalVelY += particle.getVelY();
        }
        totalVelX /= particles.size();
        totalVelY /= particles.size();
        return Math.sqrt(totalVelX * totalVelX + totalVelY * totalVelY);
    }

    private void updateStates(double ETA) {
        Set<Map.Entry<Particle, Set<Particle>>> entries = neighborMap.entrySet();
        Map<Particle, Double> nextVel = new HashMap<>();

        for (Map.Entry entry : entries) {
            Particle current = (Particle) entry.getKey();
            double xAcu = current.getVelX(), yAcu = current.getVelY(), angle;
            Set<Particle> neighbors = (Set<Particle>) entry.getValue();
            if (USE_WEIGHTS) {
                for (Particle neighbor : neighbors) {
                    double weight = neighbor.getRadius();
                    xAcu += neighbor.getVelX() * weight;
                    yAcu += neighbor.getVelY() * weight;
                }
            } else {
                for (Particle neighbor : neighbors) {
                    xAcu += neighbor.getVelX();
                    yAcu += neighbor.getVelY();
                }
            }

            angle = Math.atan2(xAcu, yAcu);

            //xAcu /= (neighbors.size() + 1);
            //yAcu /= (neighbors.size() + 1);


            if (USE_ETA) {
                double random = randomAngle(ETA);
                angle = (angle + random + Math.PI) % (2 * Math.PI) - Math.PI;
            }

            nextVel.put(current, angle);
        }

        for (Map.Entry entry : nextVel.entrySet()) {
            Particle current = (Particle) entry.getKey();
            current.updateAngle((Double) entry.getValue());
        }
    }


    private void moveParticles(List<Particle>[][] matrix, double L, double side) {

        for (Particle p : particles) {
            double oldX = p.getX();
            double oldY = p.getY();

            p.setX((oldX + p.getVelX()) % L);
            p.setY((oldY + p.getVelY()) % L);

            if (p.getX() < 0) {
                p.setX(p.getX() + L);
            }
            if (p.getY() < 0) {
                p.setY(p.getY() + L);
            }

            if (((int) Math.floor(p.getX() / side) != (int) Math.floor(oldX / side)) ||
                    ((int) Math.floor(p.getY() / side) != (int) Math.floor(oldY / side))) {

                matrix[(int) Math.floor(oldX / side)][(int) Math.floor(oldY / side)].remove(p);
                matrix[(int) Math.floor(p.getX() / side)][(int) Math.floor(p.getY() / side)].add(p);
            }
        }
    }


    private void checkInfluence(List<Particle>[][] matrix, double rc, double L, boolean periodicBorderCondition) {
        neighborMap = new HashMap<>();

        for (Particle p : particles) {
            neighborMap.put(p, new HashSet<>());
        }

        for (int indexX = 0; indexX < matrix.length; indexX++) {
            for (int indexY = 0; indexY < matrix[0].length; indexY++) {
                checkSelfNeighbor(matrix[indexX][indexY], rc, L);

                if (indexY > 0) {
                    checkNeighbor(matrix[indexX][indexY], matrix[indexX][indexY - 1], rc, 0, 0);
                    if (indexX < matrix.length - 1) {
                        checkNeighbor(matrix[indexX][indexY], matrix[indexX + 1][indexY - 1], rc, 0, 0);
                    }
                }
                if (indexX < matrix.length - 1) {
                    checkNeighbor(matrix[indexX][indexY], matrix[indexX + 1][indexY], rc, 0, 0);
                    if (indexY < matrix[0].length - 1) {
                        checkNeighbor(matrix[indexX][indexY], matrix[indexX + 1][indexY + 1], rc, 0, 0);
                    }
                }

                if (periodicBorderCondition) {
                    if (indexY == 0) {
                        checkNeighbor(matrix[indexX][indexY], matrix[indexX][matrix[0].length - 1], rc, 0, -L);
                        if (indexX == matrix.length - 1) {
                            checkNeighbor(matrix[indexX][indexY], matrix[0][matrix[0].length - 1], rc, L, -L);
                        } else {
                            checkNeighbor(matrix[indexX][indexY], matrix[indexX + 1][matrix[0].length - 1], rc, L, -L);
                        }
                    }
                    if (indexX == matrix.length - 1) {
                        checkNeighbor(matrix[indexX][indexY], matrix[0][indexY], rc, L, 0);
                        if (indexY > 0) {
                            checkNeighbor(matrix[indexX][indexY], matrix[0][indexY - 1], rc, L, 0);
                        }
                        if (indexY < matrix[0].length - 1) {
                            checkNeighbor(matrix[indexX][indexY], matrix[0][indexY + 1], rc, L, 0);
                        } else {
                            checkNeighbor(matrix[indexX][indexY], matrix[0][0], rc, L, L);
                        }
                    } else if (indexY == matrix.length - 1) {
                        checkNeighbor(matrix[indexX][indexY], matrix[indexX + 1][0], rc, L, L);
                    }
                }
            }
        }
    }

    private void checkSelfNeighbor(List<Particle> pList1, double rc, double L) {
        for (int i = 0; i < pList1.size(); i++) {
            Particle p1 = pList1.get(i);
            for (int j = i + 1; j < pList1.size(); j++) {
                Particle p2 = pList1.get(j);
                evaluateParticles(p1, p2, rc, 0, 0);
            }
        }
    }

    private void checkNeighbor(List<Particle> pList1, List<Particle> pList2, double rc, double XShift, double YShift) {
        for (Particle p1 : pList1) {
            for (Particle p2 : pList2) {
                evaluateParticles(p1, p2, rc, XShift, YShift);
            }
        }
    }

    private void evaluateParticles(Particle p1, Particle p2, double rc, double XShift, double YShift) {
        double p1X, p2X, p1Y, p2Y, distance;
        if (p1.equals(p2)) {
            return;
        }
        p1X = p1.getX();
        p2X = p2.getX() + XShift;
        p1Y = p1.getY();
        p2Y = p2.getY() + YShift;

        distance = Math.pow(p1X - p2X, 2) + Math.pow(p1Y - p2Y, 2);

        if (distance < Math.pow(rc + p1.getRadius() + p2.getRadius(), 2)) {
            neighborMap.get(p1).add(p2);
            neighborMap.get(p2).add(p1);
        }

    }

    private double randomAngle(double ETA) {
        return (Math.random() * ETA - ETA / 2) % (2 * Math.PI);
    }
}
